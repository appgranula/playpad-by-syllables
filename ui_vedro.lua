module(..., package.seeall)

math.randomseed(os.time())

local function reveal(target)
    target.alpha = 0
 
    transition.to(target, {time = 500, alpha = 1}) 
end



local function cloaking(target)
 
    transition.to(target, {time = 500, alpha = 0, onComplete = function() display.remove(target); target = nil; end})
end

local function cloak_alert(list)
    for i = 1, #list do  
        cloaking(list[i])
    end
end

function display_alert(group, message)
   
    local message = display.newText(message,0 , 0, "Flow", 24)
    -- message:setReferencePoint( display.CenterReferencePoint )
    message.anchorX = 0.5
    message.anchorY = 0.5
    message.x, message.y = display.contentCenterX, display.contentHeight - 150
    
    
    print("message.x " .. message.x)
    print("message.y " .. message.y)
    print("message.width " .. message.width)
    print("message.height " .. message.height)
    group:insert(message)
    local left, top, width, height = message.x - message.width * 0.5 - 10, 
                        message.y  - message.height * 0.5 - 10 ,
                        message.width + 20,
                        message.height +20
    print(left .. " left " .. top  .. " top " ..  width .. " width ".. height .." height")
    local message_background = display.newRect(left,top,width, height)
    message_background.anchorX = 0
    message_background.anchorY = 0
    message_background:setFillColor(0.5469, 0.5469, 0.5469)
    group:insert(message_background)
    message:toFront()
    reveal(message_background)
    reveal(message)
    
    local list = {message, message_background}
    local my_closure = function() return cloak_alert(list) end
    timer.performWithDelay(2000, my_closure, 1) --, 1)
    
end

