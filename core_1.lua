module(..., package.seeall)

math.randomseed(os.time())

--  храним слоги
syllables = {}
table.insert(syllables,0, "ма")
table.insert(syllables,1, "па")
table.insert(syllables,2, "ры")
table.insert(syllables,3, "ба")
table.insert(syllables,4, "за")
table.insert(syllables,5, "яц")
table.insert(syllables,6, "чаш")
table.insert(syllables,7, "ка")
table.insert(syllables,8, "кош")
table.insert(syllables,9, "вил")
table.insert(syllables,10, "со")
table.insert(syllables,11, "")
table.insert(syllables,12, "ра")
table.insert(syllables,13, "ке")
table.insert(syllables,14, "та")
table.insert(syllables,15, "ду")
table.insert(syllables,16, "га")
table.insert(syllables,17, "де")
table.insert(syllables,18, "ре")
table.insert(syllables,19, "во")
table.insert(syllables,20, "ко")
table.insert(syllables,21, "ро")
table.insert(syllables,22, "ва")
table.insert(syllables,23, "са")
table.insert(syllables,24, "мо")
table.insert(syllables,25, "лет")
table.insert(syllables,26, "гру")
table.insert(syllables,27, "шют")
table.insert(syllables,28, "ход")
table.insert(syllables,29, "об")
table.insert(syllables,30, "ла")
table.insert(syllables,31, "я")
table.insert(syllables,32, "но")
table.insert(syllables,33, "рог")
table.insert(syllables,34, "лит")
table.insert(syllables,35, "ран")
table.insert(syllables,36, "даш")
table.insert(syllables,37, "воч")
table.insert(syllables,38, "фо")
table.insert(syllables,39, "кус")
table.insert(syllables,40, "ник")
table.insert(syllables,41, "кар")
table.insert(syllables,42, "тош")
table.insert(syllables,43, "кро")
table.insert(syllables,44, "дил")
table.insert(syllables,45, "рас")
table.insert(syllables,46, "чес")
table.insert(syllables,47, "ман")
table.insert(syllables,48, "да")
table.insert(syllables,49, "рин")
table.insert(syllables,50, "ан")
table.insert(syllables,51, "ти")
table.insert(syllables,52, "ло")
table.insert(syllables,53, "зо")
table.insert(syllables,54, "вик")
table.insert(syllables,55, "о")
table.insert(syllables,56, "бе")
table.insert(syllables,57, "зья")
table.insert(syllables,58, "на")
table.insert(syllables,59, "пи")
table.insert(syllables,60, "а")
table.insert(syllables,61, "ни")
table.insert(syllables,62, "фе")
table.insert(syllables,63, "вар")
table.insert(syllables,64, "ле")
table.insert(syllables,65, "ве")
table.insert(syllables,66, "си")
table.insert(syllables,67, "пед")
table.insert(syllables,68, "ри")
table.insert(syllables,69, "те")
table.insert(syllables,70, "ви")
table.insert(syllables,71, "зор")
table.insert(syllables,72, "шо")
table.insert(syllables,73, "лад")
table.insert(syllables,74, "экс")
table.insert(syllables,75, "тор")
table.insert(syllables,76, "тре")
table.insert(syllables,77, "у")
table.insert(syllables,78, "голь")
table.insert(syllables,79, "ми")
table.insert(syllables,80, "че")
table.insert(syllables,81, "ха")
table.insert(syllables,82, "ав")
table.insert(syllables,83, "то")
table.insert(syllables,84, "биль")
table.insert(syllables,85, "по")
table.insert(syllables,86, "се")
table.insert(syllables,87, "нок")
table.insert(syllables,88, "тен")
table.insert(syllables,89, "це")
table.insert(syllables,90, "хо")
table.insert(syllables,91, "диль")
table.insert(syllables,92, "лай")
table.insert(syllables,93, "ап")
table.insert(syllables,94, "рат")
table.insert(syllables,95, "ца")
table.insert(syllables,96, "же")
table.insert(syllables,97, "е")
table.insert(syllables,98, "ог")
table.insert(syllables,99, "не")
table.insert(syllables,100, "ту")
table.insert(syllables,101, "ши")
table.insert(syllables,102, "тель")


--  храним слова и номера слогов, из которых они состоят
words = {}
table.insert(words,0, {"мама", 0, 0}) -- 0 - номер из syllables таблицы
table.insert(words,1, {"папа", 1, 1})			
table.insert(words,2, {"рыба", 2, 3})			
table.insert(words,3, {"заяц", 4, 5})			
table.insert(words,4, {"чашка", 6, 7})			
table.insert(words,5, {"кошка", 8, 7})			
table.insert(words,6, {"вилка", 9, 7})			
table.insert(words,7, {"собака", 10, 3, 7})		
table.insert(words,8, {"ракета", 12, 13, 14})		
table.insert(words,9, {"радуга", 12, 15, 16})		
table.insert(words,10, {"дерево", 17, 18, 19})		
table.insert(words,11, {"корова", 20, 21, 22})		
table.insert(words,12, {"самолет", 23, 24, 25})		
table.insert(words,13, {"парашют", 1, 12, 27})		
table.insert(words,14, {"пароход", 1, 21, 28})		
table.insert(words,15, {"облако", 29, 30, 20})		
table.insert(words,16, {"носорог", 32, 10, 33})		
table.insert(words,17, {"палитра", 1, 34, 12})		
table.insert(words,18, {"карандаш", 7, 35, 36})		
table.insert(words,19, {"девочка", 17, 37, 7})		
table.insert(words,20, {"фокусник", 38, 39, 40})		
table.insert(words,21, {"картошка", 41, 42, 7})		
table.insert(words,22, {"крокодил", 43, 20, 44})		
table.insert(words,23, {"расческа", 45, 46, 7})		
table.insert(words,24, {"мандарин", 47, 48, 49})		
table.insert(words,25, {"грузовик", 26, 53, 54})		
table.insert(words,26, {"антилопа", 50, 51, 52, 1})	
table.insert(words,27, {"обезьяна", 55, 56, 57, 58})	
table.insert(words,28, {"пианино", 59, 60, 61, 32})	
table.insert(words,29, {"кофеварка", 20, 62, 63, 7})	
table.insert(words,30, {"королева", 20, 21, 64, 22})	
table.insert(words,31, {"велосипед", 65, 52, 66, 67})	
table.insert(words,32, {"балерина", 3, 64, 68, 58})	
table.insert(words,33, {"телевизор", 69, 64, 70, 71})	
table.insert(words,34, {"шоколадка", 72, 20, 73, 7})	
table.insert(words,35, {"экскаватор", 74, 7, 22, 75})	
table.insert(words,36, {"треугольник", 76, 77, 78, 40})	
table.insert(words,37, {"пирамида", 59, 12, 79, 48})	
table.insert(words,38, {"черепаха", 80, 18, 1, 81})	
table.insert(words,39, {"автомобиль", 82, 83, 24, 84})	
table.insert(words,40, {"поросенок", 85, 21, 86, 87})	
table.insert(words,41, {"полотенце", 85, 52, 88, 89})	
table.insert(words,42, {"холодильник", 90, 52, 91, 40})	
table.insert(words,43, {"балалайка", 3, 30, 92, 7})	
table.insert(words,44, {"одеяло", 55, 17, 31, 52})	
table.insert(words,45, {"математика", 0, 69, 0, 51, 7})
table.insert(words,46, {"фотоаппарат", 38, 83, 93, 1, 94})
table.insert(words,47, {"каракатица", 7, 12, 7, 51, 95})
table.insert(words,48, {"мороженое", 24, 21, 96, 32, 97})
table.insert(words,49, {"огнетушитель", 98, 99, 100, 101, 102})


function compare_tables(t1, t2)
  if #t1 ~= #t2 then return false end
  for i=1,#t1 do
    if t1[i] ~= t2[i] then return false end
  end
  return true
end

level = 0

local l_group -- группа из createScene
local image_background, d_gray, d_color

local detrituses = {} --  слоги

local physics = require("physics")
physics.start()
physics.setGravity(0, 0) -- чтоб вниз не падали 0  по X и 0 по Y ускорение ( смотрим как бы сверху, а не сбоку)

--  координаты, когда активируется стазис
local stasis_x = {}


table.insert(stasis_x, 2, {120, 360})
table.insert(stasis_x, 3, {80, 240, 400})
table.insert(stasis_x, 4, {60, 180, 300, 420})
table.insert(stasis_x, 5, {48, 144, 240, 336, 432})
local stasis_y = 730

--  в каком диапазоне ищем стазисные положения
local radius_x = {}
table.insert(radius_x, 2, 110)
table.insert(radius_x, 3, 80)
table.insert(radius_x, 4, 60)
table.insert(radius_x, 5, 40)
local radius_y = 40


 stasis_count = 0 -- число в стазисе
 perfect_stasis = {}
 current_stasis = {}
local function check_stasis(x, y)
 
end


--  пройден уровень 
function level_complete()
    transition.to(d_gray, {time = 1000, alpha = 0, onComplete = function() display.remove(d_gray); d_gray = nil end})
    
    d_color = display.newImage("images/".. level .. "_color.png")
    -- d_color:setReferencePoint( display.CenterReferencePoint )
    d_color.anchorX = 0.5
    d_color.anchorY = 0.5
    d_color.x, d_color.y = display.contentCenterX, display.contentCenterY
    d_color.alpha = 0
    
    --d_color:toBack()
   -- image_background:toBack()
    l_group:insert(d_color)

    transition.to(d_color, {time = 1000, alpha = 1})
    
end

local function ressurect()
    stasis_count = 0 -- число в стазисе
     perfect_stasis = {}
    current_stasis = {}
    detrituses = {}
end

local function check_if_any_detritus_already_in_this_position(i)
    local already_used = false
    for k = 1, #detrituses do
        if detrituses[k].now_at == i then
            already_used = true
        end
    end
    
    return already_used
end

--  уничтожаем прилепленный и показываем непрелепленный
local function handler_click(event)
    local t = event.target
    local phase = event.phase
    local r = t.link_to_target
    print("1")
    if "ended" == phase then 
        if t.clicked and r.now_at == -1 then
        print("!")
        r.alpha = 0
        t.alpha = 1
        t.clicked = false
         
         
           r.isBodyActive = false
            r.now_at = t.now_at
            r.nead_value_to_be_here = t.nead_value_to_be_here -- ушли из ячеки и забываем что ей нужно
            -- выели из стазиса
           
                stasis_count = stasis_count + 1
            local l =  #words[level] 
            if stasis_count == l - 1 then
                                    print("all in stasis")
                                    local all_good = true
                                    for v = 1, #detrituses do
                                        -- проверяем по значениям
                                        if detrituses[v].nead_value_to_be_here ~= detrituses[v].value then
                                            
                                            all_good = false
                                        end
                                    end
                                    if all_good then
                                        print("yeeah")
                                        level_complete()
                                        native.showAlert("!", "yeeah")
                                    else 
                                        print("ohhh")
                                        native.showAlert("!", "ohhh")
                                    end
                                    
                                    
                                end
            return true
        end
    
    t.alpha = 0.01
    t.now_at = -1
   -- display.remove(t)
   -- t = nil -- скрываем прилепленный
    --[[
    if r.touched == true then
        return true
    end
    r.touched = true
     ]]--
   -- transition.to(r, {time = 1000, alpha = 1, onComplete = function () r.touched = false end})-- раскрываем непрелепленный
    r.alpha = 1 
    
    t.clicked = true
    
    if r.isBodyActive == false then
            --  даем ему возможность быть активным
            r.isBodyActive = true
            r.now_at = -1
            r.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
            -- выели из стазиса
            if stasis_count > 0 then
                stasis_count = stasis_count - 1
            end
            
            
          
        end
   end
   return true
end

--  скрываем непрелепленный и показываем прелепленный
local function reveal_in_stasis(target)
  
   target.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
   local path_to_image = target.path .. "_1.png"
       -- print(str)
   local d = display.newImage(path_to_image)
   if target.d ~= nil then
       display.remove(target.d )
   end
   target.d = d
   d.clicked = false
   d.x, d.y = target.x, target.y
   d.link_to_target = target
   d.now_at = target.now_at
   d.nead_value_to_be_here = target.nead_value_to_be_here
   d:addEventListener("touch", handler_click)
   l_group:insert(d)
   
    target.alpha = 0
   target.now_at = -1
   target.x, target.y = math.random(90, 400), math.random(260, 650)
   --target.alpha = 0
end

-- A basic function for dragging physics objects
local function startDrag( event )
        local t = event.target
 
        local phase = event.phase
        
       -- print("\n\nbefore " .. stasis_count)
        
        --  кликаем по слогу, уже закрепленному внизу
        if t.isBodyActive == false then
            --  даем ему возможность быть активным
            t.isBodyActive = true
            t.now_at = -1
            t.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
            -- выели из стазиса
            if stasis_count > 0 then
                stasis_count = stasis_count - 1
            end
            
          
        end
        --------
        --print("after " .. stasis_count )
   
        
        
        if "began" == phase then
            
                display.getCurrentStage():setFocus( t )
                t.isFocus = true
 
                for i = 1, #detrituses do
                    if i ~= t.k then
                        detrituses[i].bodyType = "static"
                    end
                end
                t:toFront()   -- терь вверху группы    
                                
                -- Store initial position
                t.x0 = event.x - t.x
                t.y0 = event.y - t.y
                
                -- Make body type temporarily "kinematic" (to avoid gravitional forces)
               event.target.bodyType = "dynamic"
                
                -- Stop current motion, if any
                event.target:setLinearVelocity( 0, 0 )
                event.target.angularVelocity = 0
 
        elseif t.isFocus then
                if "moved" == phase then
                        t.x = event.x - t.x0
                        t.y = event.y - t.y0
                                
                        --  проверка чтобы не вылезло за границы при драге        
                        if t.x < 10 then t.x = 10
                        end
                        if t.x > 470 then t.x = 470
                        end
                        if t.y < 10 then t.y = 10
                        end
                        if t.y > 790 then t.y = 790
                        end    
                        event.target.bodyType = "kinematic"    -- игнорирование гравитации
                        
                elseif "ended" == phase or "cancelled" == phase then
                        display.getCurrentStage():setFocus( nil )
                        t.isFocus = false
                        
                        -- Switch body type back to "dynamic", unless we've marked this sprite as a platform
                        if ( not event.target.isPlatform ) then
                                event.target.bodyType = "dynamic"
                        end
                        
                        
                        ------------------------------------------------------
                      --  print("t.x " .. t.x)
                     --   print("t.y " .. t.y)
                        
                        local l =  #words[level] -- {"математика", 0, 69, 0, 51, 7})
                        for i = 1, l-1 do
                            local x_ = stasis_x[#words[level] - 1][i]
                           -- print("x_ " .. x_)
          
                            if math.abs(t.x - x_) < radius_x[#words[level] - 1] and math.abs(t.y - stasis_y) < radius_y then
                               -- print("in stasis")
                               -- проверка на свободность ячейки
                               if check_if_any_detritus_already_in_this_position(i) == false then
                                t.x = x_
                                t.y = stasis_y
                                --t:removeEventListener("touch", startDrag)
                                --t.bodyType = "static"
                                
                                reveal_in_stasis(t)
                                
                                --  в стазисе слог не олжен взаимодействовать с другими до клика: пока по нему не кликнут
                                t.isBodyActive = false
                                --  еще 1 ячейка в стазисе
                                stasis_count = stasis_count + 1
                               
                                
                                t.now_at = i -- запоминаем в какой ячейке сейчас находимся: чтобы при передвижении других знать, что эта ячейка занята
                   
                               -- вошли в ячейку и запоминаем какое значение должно быть НА САМОМ ДЕЛЕ (нужно) ячейке
                                t.nead_value_to_be_here = words[level][i+1]
                                
                                -- проверка на победу: если для всех слогов в ячейках 
                                -- значение нужное ячейке совпадает со значением слога
                                if stasis_count == l - 1 then
                                    print("all in stasis")
                                    local all_good = true
                                    for v = 1, #detrituses do
                                        -- проверяем по значениям
                                        if detrituses[v].nead_value_to_be_here ~= detrituses[v].value then
                                            
                                            all_good = false
                                        end
                                    end
                                    if all_good then
                                        print("yeeah")
                                        level_complete()
                                        native.showAlert("!", "yeeah")
                                    else 
                                        print("ohhh")
                                        native.showAlert("!", "ohhh")
                                    end
                                    
                                    
                                end
                            end
                        end
                        
                        end
                        ------------------------------------------------------
                        
                        
                        for i = 1, #detrituses do
                            if i ~= t.k then
                                detrituses[i].bodyType = "dynamic"
                            end
                        end
 
                end
        end
 
        -- Stop further propagation of touch event!
        return true
end


--  случайно размесить слоги
function level_randomly_seed_syllables(group)
    local d_count = #words[level] -- первое будет само слово, образуемое слагами
    for i = 2, d_count do           -- поэтому начинаем с 2, а не с 1
       -- print(level)
      --  print(i)
        
        local path_to_image = "images/sl" .. words[level][i] .. ".png"
       -- print(str)
        local d = display.newImage(path_to_image)
        d.x, d.y = math.random(90, 400), math.random(260, 650)
        d.k = i - 1
       -- d.k = words[level][i]
       d.value = words[level][i]
        d.path = "images/sl" .. words[level][i]
        d.number = words[level][i]
        d.alpha = 0
        transition.to(d, {time = 1000, alpha = 1})-- раскрываем непрелепленный
       
        table.insert(detrituses, d)
        table.insert(perfect_stasis, words[level][i]) -- запоминаем номера слогов
        d:addEventListener( "touch", startDrag )
        physics.addBody(d, "dynamic")
         d.isFixedRotation = true
        group:insert(d)
    end
        
end
--[[
function load_walls()
    local _W = display.contentWidth
    local _H = display.contentHeight
    local left_wall = display.newRect( -50, 0, 1, _H * 2)
    physics.addBody(left_wall, "static")

    local right_wall = display.newRect( _W + 10, 0, 50, _H * 2)
    physics.addBody(right_wall, "static")

    local up_wall = display.newRect(0, -10, _W, 20)
    physics.addBody(up_wall, "static")
    
    local down_wall = display.newRect(0, _H + 10, _W, 20)
    physics.addBody(down_wall, "static")
    
    l_group:insert(left_wall)
    l_group:insert(right_wall)
    l_group:insert(up_wall)
    l_group:insert(down_wall)
end
]]--
--  загрузка бекграунда и слогов
function load_level_data(group)
    l_group = group
    ressurect()    -- очищаем от предыдущего игрового уровня: различные таблицы становятся пустыми
    --  бекграунд
    image_background = display.newImage( "images/background.png")
    -- image_background:setReferencePoint( display.CenterReferencePoint )
    image_background.anchorX = 0.5
    image_background.anchorY = 0.5
    image_background.x, image_background.y = display.contentCenterX, display.contentCenterY  
    group:insert(image_background)
     
    -- серая картинка (заменится на ицветную в случае победы) 
    d_gray = display.newImage("images/".. level .. ".png")
    -- d_gray:setReferencePoint( display.CenterReferencePoint )
    d_gray.anchorX = 0.5
    d_gray.anchorY = 0.5
    d_gray.x, d_gray.y = display.contentCenterX, display.contentCenterY
    group:insert(d_gray)
    
    --  размещение статических препятствий вокруг экрана, чтоб слоги не вылезали
   -- create wall objects
    local topWall = display.newRect( -40, -40, display.contentWidth, -60 )
    local bottomWall = display.newRect( -40, display.contentHeight + 40 , display.contentWidth, display.contentHeight + 60 )
    local leftWall = display.newRect( -40, -40, -30, display.contentHeight + 40 )
    local rightWall = display.newRect( display.contentWidth + 30, 0, display.contentWidth + 60, display.contentHeight  )

    -- make them physics bodies
    physics.addBody(topWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(bottomWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(leftWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(rightWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    
    --  случайное размещение слогов + dynamic физика на них
    level_randomly_seed_syllables(group)
end

--  проверка на правильность расположения слогов
function level_check()
    
end


