----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local core = require("core")
local tableView = require("tableView")

--import the button events library
local ui = require("ui")
local widget = require "widget"

local ui_vedro = require("ui_vedro")

local data_lang = require("local_" .. storyboard.lang)

----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------





local l_group
--setup functions to execute on touch of the list view items
function listButtonRelease( event )
	self = event.target
        
        
	local id = self.id
	print(self.id)
        if (id - 1) <= storyboard.json_progress.level  then
            storyboard.level = id - 1
        else
            ui_vedro.display_alert(l_group, storyboard.translations["achieve_more"])
            return true
        end    
            
        --self.delta, self.velocity = 0, 0
        
        storyboard.gotoScene( "action" )
	--[[
	detailScreenText.text = "You tapped item ".. self.id
			
	transition.to(myList, {time=400, x=display.contentWidth*-1, transition=easing.outExpo })
	transition.to(detailScreen, {time=400, x=0, transition=easing.outExpo })
	transition.to(backBtn, {time=400, x=math.floor(backBtn.width/2) + screenOffsetW*.5 + 6, transition=easing.outExpo })
	transition.to(backBtn, {time=400, alpha=1 })
	
	delta, velocity = 0, 0
        ]]--
        return true
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
        print("created level_chooser")
        
	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        
        --initial values
        local screenOffsetW, screenOffsetH = display.contentWidth -  display.viewableContentWidth, display.contentHeight - display.viewableContentHeight

        local myList, backBtn, detailScreenText

        --local background = display.newImage("images/background_main.png", true)
        local background = display.newImage("images/background_main.png");
        background.x = display.contentCenterX
        background.y = display.contentCenterY
        
        group:insert(background)
  
        data = {}
        
        local max_achieved_level = storyboard.json_progress.level   

        for i = 0, #data_lang.words do
            if i <= max_achieved_level then
                table.insert(data, {image = "images/modified/".. data_lang.words[i]["path"] .. "_color.png"})
            else
                table.insert(data, {image = "images/modified/".. data_lang.words[i]["path"] .. ".png"})
            end        
        end
        
        local myList = tableView.newList
        {
            data=data,
            default="images/listItemBg___.png",
           -- backgroundColor={255,255,255},
           top = 50,
           bottom = 50,
           onRelease=listButtonRelease,
            callback = function( row )
                local g = display.newGroup()

                local img = display.newImage(row.image)
                img:scale(0.8, 0.8)
                img.x = display.contentWidth / 2 -- img.width * 0.5 + 20
                g:insert(img)
              --  img.x = math.floor(img.width*0.5 * 0.5 + 60)
               -- img.y = math.floor(img.height*0.5 * 0.5)
                --[[
                local title = display.newText(row.title, 0, 0, native.systemFontBold, 14)
                title:setTextColor(255,255,255)
                g:insert(title)
                title.x = math.floor(title.width*0.5) + img.width + 6
                title.y = 30

                local subtitle = display.newText(row.subtitle, 0, 0, native.systemFont, 12)
                subtitle:setTextColor(180, 180, 180)
                g:insert(subtitle)
                subtitle.x = math.floor(subtitle.width*0.5) + img.width + 6 
                subtitle.y = title.y + title.height + 6
                                ]]--
               
                --[[
                local t = display.newImage("images/listItemBg.png")
                t.y = img.height + 160
                g:insert(t)
                ]]--
                return g
            end
        }
       
        group:insert(myList)
     --   ui_vedro.display_alert(group, "листай вверх-вниз")
end


local function onKeyEvent( event )
            local keyname = event.keyName;
            if (event.phase == "up" and (event.keyName=="back" or event.keyName=="menu")) then
               if keyname == "menu" then
                      
                  
               elseif keyname == "back" then
                      storyboard.gotoScene("action")
                       -- goBack();
               elseif keyname == "search" then
                        --performSearch();
               end
               return true;
            end
            return false;
        end



-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
        print("entered level_chooser")
        --ui_vedro.display_alert(group, "листай вверх-вниз")
	storyboard.purgeScene("action")
        storyboard.purgeScene("menu")
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
        l_group = group
        
         --add the runtime event listener
        if system.getInfo( "platformName" ) == "Android" then  Runtime:addEventListener( "key", onKeyEvent ) end


	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	print("exit level_chooser")
	if system.getInfo( "platformName" ) == "Android" then  Runtime:removeEventListener( "key", onKeyEvent ) end

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene

