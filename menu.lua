

----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require("widget")

----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------


local alert_group = {}
local alert_opened = false 
 
 
 local function check_audio_on_or_off()
    if storyboard.json_progress.audio == 1 then
        return true
    end
    return false
end

local function play_click()
    if check_audio_on_or_off() == false then
        return;
    end
    local d = audio.loadStream("audio/button.mp3")
    audio.play(d)     
end
 
local function close_alert()
    play_click()
    for i=1, #alert_group do
        transition.to(alert_group[i], {time = 500,  alpha = 0, onComplete = function(event) alert_group[i]=nil end})
    end
    transition.to(global_background, {time = 500,  alpha = 0, onComplete = function(event) global_background=nil end})
    alert_opened = false
end
 
 
  
local function onKeyEvent( event )
       local keyname = event.keyName;
     -- native.showAlert("!", "menu handler")
     if  keyname == "back" then
            os.exit()
            return true
               -- backButtonPushed = true
         --   return true
        end
            return false;
end
 
local function show_about_alert(event)
    alert_opened = true
    local group = event.target.group
    
    global_background = display.newRect( - display.contentWidth , - display.contentHeight , display.contentWidth * 4 ,  display.contentHeight * 4)

    global_background:setFillColor(0.0586, 0.0586, 0.0586)
    global_background.alpha = 0.8
    
    
    local alert_top = display.newImage("images/alert2_top.png")
    -- alert_top:setReferencePoint( display.CenterReferencePoint )
    alert_top.anchorX = 0.5
    alert_top.anchorY = 0.5
    alert_top.x, alert_top.y = display.contentCenterX, display.contentCenterY -20
    
    
    local btn_ok = widget.newButton{
                label = storyboard.translations["OK"],
		defaultFile="images/alert2_bottom_empty.png",
		overFile="images/alert2_bottom_empty.png",
                font = native.systemFont,
                fontSize = 22,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 0 } },
		onRelease = close_alert	-- event listener function
	}
        -- btn_ok:setReferencePoint( display.CenterReferencePoint )
        btn_ok.anchorX = 0.5
        btn_ok.anchorY = 0.5
	btn_ok.x, btn_ok.y = display.contentCenterX , display.contentCenterY + 100
  
    local image_icon = display.newImageRect("Icon.png", 115, 115)
    -- image_icon:setReferencePoint( display.CenterReferencePoint )
    image_icon.anchorX = 0.5
    image_icon.anchorY = 0.5
    image_icon.x, image_icon.y = display.contentCenterX - 140, display.contentCenterY +10
    
    local a = display.newText(storyboard.translations["about_header"], display.contentCenterX - 180, display.contentCenterY - 95, native.systemFont, 18)
    a.anchorX = 0
    a.anchorY = 0
    local b = display.newText(storyboard.translations["about_text"], display.contentCenterX - 71,  display.contentCenterY - 45, 270, 0, native.systemFont, 12)
    b.anchorX = 0
    b.anchorY = 0
    
    table.insert(alert_group, alert_top)
    table.insert(alert_group, btn_ok)
    table.insert(alert_group, image_icon)    
    table.insert(alert_group, a)    
    table.insert(alert_group, b)

    for i=1, #alert_group do
        alert_group[i].alpha = 0
    end
    
    
    for i=1, #alert_group do
        transition.to(alert_group[i], {time = 500,  alpha = 1})
    
    end

    
    group:insert(global_background)
    
    group:insert(alert_top)
    group:insert(btn_ok)
    group:insert(image_icon)
    group:insert(a)
    group:insert(b) 
end
 
 
 local function onAboutBtnRelease(event)
        play_click()
	-- go to level1.lua scene
	--native.showAlert("О программе", "Программа «Собери осколки» специально создана для детского планшетного компьютера PLAYPAD ®  ©  APPGRANULA, 2012")
        --native.showAlert(storyboard.translations["О программе"][storyboard.language], storyboard.translations["longest about"][storyboard.language], {storyboard.translations["OK"][storyboard.language]}, onComplete);
        show_about_alert(event)
	return true	-- indicates successful touch
end


-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
        --timer.performWithDelay(3000, function() storyboard.can = true end)
	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        
        local background = display.newImage("images/background_main.png")
        -- background:setReferencePoint(display.CenterReferencePoint)
        background.anchorX = 0.5
        background.anchorY = 0.5
        background.x, background.y = display.contentCenterX, display.contentCenterY
        
        local btn_action = widget.newButton{
              
		defaultFile="images/ui-" .. storyboard.lang .. "/back1.png",
		overFile="images/ui-" .. storyboard.lang .. "/back1.png",
                --font = "Lobster",
                --fontSize = 44,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 1 } },
                onRelease = function()  storyboard.gotoScene("action"); play_click(); return true; end
	}
        -- btn_action:setReferencePoint( display.CenterReferencePoint )
        btn_action.anchorX = 0.5
        btn_action.anchorY = 0.5
        btn_action.x, btn_action.y = display.contentCenterX, display.contentHeight * 0.2


        local btn_levels = widget.newButton{
              
        defaultFile="images/ui-" .. storyboard.lang .. "/back4.png",
        overFile="images/ui-" .. storyboard.lang .. "/back4.png",
                --font = "Lobster",
                --fontSize = 44,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 1 } },
                onRelease = function()  storyboard.gotoScene("level_chooser"); play_click(); return true; end
    }
        -- btn_action:setReferencePoint( display.CenterReferencePoint )
        btn_levels.anchorX = 0.5
        btn_levels.anchorY = 0.5
        btn_levels.x, btn_levels.y = display.contentCenterX, display.contentHeight * 0.4
        
        
        --btn_action.rotation = 90
        local btn_about = widget.newButton{
  
		defaultFile="images/ui-" .. storyboard.lang .. "/back2.png",
		overFile="images/ui-" .. storyboard.lang .. "/back2.png",
                --font = "Lobster",
                --fontSize = 44,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 1 } },
                onRelease = onAboutBtnRelease
	}
        -- btn_about:setReferencePoint( display.CenterReferencePoint )
        btn_about.anchorX = 0.5
        btn_about.anchorY = 0.5
        btn_about.x, btn_about.y = display.contentCenterX, display.contentHeight * 0.6
        btn_about.group = group

        local btn_link = widget.newButton{

		defaultFile="images/back3.png",
		overFile="images/back3.png",
                --font = "Lobster",
                --fontSize = 44,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 1, 1, 1, 1 } },
                onRelease = function()  play_click(); system.openURL( "http://www.playpads.net" ); return true; end
	}
        -- btn_link:setReferencePoint( display.CenterReferencePoint )
        btn_link.anchorX = 0.5
        btn_link.anchorY = 0.5
        btn_link.x, btn_link.y = display.contentCenterX, display.contentHeight * 0.8
        
        group:insert(background)   
        group:insert(btn_action)
        group:insert(btn_levels)
        group:insert(btn_about)
        group:insert(btn_link)
end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
	-- load menu screen
        
         storyboard.purgeScene("action")
        storyboard.purgeScene("transition_scene")
        --[[
        if  storyboard.can == false then
            native.showAlert("!","false");
        
        end]]--
       --timer.performWithDelay(10000, function() storyboard.can = true end)
        --storyboard.gotoScene( "action" )
            -- storyboard.gotoScene( "level_chooser" )
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
	     --add the runtime event listener
        if system.getInfo( "platformName" ) == "Android" then  Runtime:addEventListener( "key", onKeyEvent ) end


end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
	  if system.getInfo( "platformName" ) == "Android" then  Runtime:removeEventListener( "key", onKeyEvent ) end

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene