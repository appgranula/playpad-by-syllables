

----------------------------------------------------------------------------------
--
-- scenetemplate.lua
--
----------------------------------------------------------------------------------

local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local widget = require "widget"

local core = require("core")
local loadsave = require("loadsave")

local ui_vedro = require("ui_vedro")
----------------------------------------------------------------------------------
-- 
--	NOTE:
--	
--	Code outside of listener functions (below) will only be executed once,
--	unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------

---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- Called when the scene's view does not exist:
function scene:createScene( event )
	local group = self.view
print("created action")
	-----------------------------------------------------------------------------
		
	--	CREATE display objects and add them to 'group' here.
	--	Example use-case: Restore 'group' from previously saved state.
	
	-----------------------------------------------------------------------------
        
        
        core.level = storyboard.level
        core.load_level_data(group)
        print("stasis_count " .. core.stasis_count)
        
        --core.level_complete()


end

local opt_displayed = false

local function reveal(target)
    target.alpha = 0
    opt_displayed = true
    transition.to(target, {time = 500, alpha = 1}) 
end



local function cloaking(target)
    opt_displayed = false
    transition.to(target, {time = 500, alpha = 0, onComplete = function() display.remove(target) end})
end

local btn_options, btn_sound

local function cloak_option_menu()
    cloaking(btn_options)
    cloaking(btn_sound)
end



local function audio_changer()
    if storyboard.json_progress.audio == 1 then
        storyboard.json_progress.audio = 0
    else storyboard.json_progress.audio = 1 end

    loadsave.saveTable(storyboard.json_progress, storyboard.json_file_name) --  сохраняем прогресс на флешке
    local sound_text
    if storyboard.json_progress.audio == 1 then
        sound_text = "audio_off"
    else sound_text = "audio_on" end
    
    btn_sound:setLabel(storyboard.translations[sound_text])
end

local function display_option_menu(group)
    if opt_displayed then
        return true
    end
    btn_options = widget.newButton{
                label = storyboard.translations["level"],
		defaultFile="images/btn_opt.png",
		overFile="images/btn_opt.png",
                font = native.newFont(native.systemFont),
                fontSize = 22,
                yOffset = -5,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 0.6055, 1, 0.9375, 1 } },
		onRelease = function() cloak_option_menu(); storyboard.gotoScene("level_chooser"); return true; end	-- event listener function
	}
     -- btn_options:setReferencePoint( display.CenterReferencePoint )
     btn_options.anchorX = 0.5
     btn_options.anchorY = 0.5
     btn_options.x, btn_options.y = display.contentWidth * 0.75 , display.contentHeight - 20
     reveal(btn_options)
     group:insert(btn_options)
     
     local sound_text
    if storyboard.json_progress.audio == 1 then
        sound_text = "audio_off"
    else sound_text = "audio_on" end

     btn_sound = widget.newButton{
                label = storyboard.translations[sound_text],
		defaultFile="images/btn_opt.png",
		overFile="images/btn_opt.png",
                font = native.newFont(native.systemFont),
                fontSize = 22,
                yOffset = -5,
                labelColor = { default={ 1, 1, 1, 1 }, over={ 0.6055, 1, 0.9375, 1} },
		onRelease = function() cloak_option_menu(); audio_changer(); return true; end	-- event listener function
	}
     -- btn_sound:setReferencePoint( display.CenterReferencePoint )
     btn_sound.anchorX = 0.5
     btn_sound.anchorY = 0.5
     btn_sound.x, btn_sound.y = display.contentWidth * 0.25 , display.contentHeight - 20
     btn_sound.alpha = 0
     reveal(btn_sound)
     group:insert(btn_sound)
end

local l_group
local function onKeyEvent( event )
            local keyname = event.keyName;
            if (event.phase == "up" and (event.keyName=="back" or event.keyName=="menu")) then
               if keyname == "menu" then
                        --показываем меню опций
                        if  opt_displayed == false then
                            display_option_menu(l_group) 
                        else 
                            cloak_option_menu()
                        end
                        
                    elseif keyname == "back" then
                      storyboard.can = false 
                      --[[
                      if core.gl_clock ~= nil then
                        timer.cancel(core.gl_clock)
                      end  
                      ]]--
                      storyboard.gotoScene("menu")
                       -- goBack();
               elseif keyname == "search" then
                        --performSearch();
               end
            return true;   
            end
            return false;
        end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
	local group = self.view
        l_group = group
print("entered action")
        --  opt_displayed = false
        --  display_option_menu(group)
        
        --  ui_vedro.display_alert(group, "перетащи слоги вниз")
    
        storyboard.purgeScene("level_chooser")
        storyboard.purgeScene("menu")
	-----------------------------------------------------------------------------
		
	--	INSERT code here (e.g. start timers, load audio, start listeners, etc.)
	
	-----------------------------------------------------------------------------
        
         --add the runtime event listener
        if system.getInfo( "platformName" ) == "Android" then  Runtime:addEventListener( "key", onKeyEvent ) end



	
end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
	local group = self.view

	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)
	
	-----------------------------------------------------------------------------
        print("exit action")
        if system.getInfo( "platformName" ) == "Android" then  Runtime:removeEventListener( "key", onKeyEvent ) end


end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
	local group = self.view
	
	-----------------------------------------------------------------------------
	
	--	INSERT code here (e.g. remove listeners, widgets, save state, etc.)
	
	-----------------------------------------------------------------------------
	
end

---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

---------------------------------------------------------------------------------

return scene