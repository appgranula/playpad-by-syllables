module(..., package.seeall)



--  храним слоги
syllables = {}

table.insert(syllables,0, "mom")
table.insert(syllables,1, "my")
table.insert(syllables,2, "dad")
table.insert(syllables,3, "dy")
table.insert(syllables,99, "fi")
table.insert(syllables,4, "shes")
table.insert(syllables,5, "rab")
table.insert(syllables,6, "bit")
table.insert(syllables,7, "tea")
table.insert(syllables,8, "cup")
table.insert(syllables,9, "kit")
table.insert(syllables,10, "ty")
table.insert(syllables,11, "cut")
table.insert(syllables,12, "le")
table.insert(syllables,13, "ry")
table.insert(syllables,14, "dog")
table.insert(syllables,15, "gie")
table.insert(syllables,16, "ro")
table.insert(syllables,17, "cket")
table.insert(syllables,18, "rain")
table.insert(syllables,19, "bow")
table.insert(syllables,20, "oak")
table.insert(syllables,21, "tree")
table.insert(syllables,22, "air")
table.insert(syllables,23, "plane")
table.insert(syllables,24, "pa")
table.insert(syllables,25, "ra")
table.insert(syllables,26, "chute")
table.insert(syllables,27, "steam")
table.insert(syllables,28, "boat")
table.insert(syllables,29, "rhi")
table.insert(syllables,30, "no")
table.insert(syllables,31, "lette")
table.insert(syllables,32, "pen")
table.insert(syllables,33, "cil")
table.insert(syllables,34, "gir")
table.insert(syllables,35, "lie")
table.insert(syllables,36, "ma")
table.insert(syllables,37, "gi")
table.insert(syllables,38, "cian")
table.insert(syllables,39, "po")
table.insert(syllables,40, "ta")
table.insert(syllables,41, "to")
table.insert(syllables,42, "cro")
table.insert(syllables,43, "co")
table.insert(syllables,44, "dile")
table.insert(syllables,45, "hair")
table.insert(syllables,46, "brush")
table.insert(syllables,47, "o")
table.insert(syllables,48, "range")
table.insert(syllables,49, "lor")
table.insert(syllables,50, "an")
table.insert(syllables,51, "te")
table.insert(syllables,52, "lope")
table.insert(syllables,53, "mon")
table.insert(syllables,54, "key")
table.insert(syllables,55, "pia")
table.insert(syllables,56, "bi")
table.insert(syllables,57, "cycle")
table.insert(syllables,58, "bal")
table.insert(syllables,59, "let")
table.insert(syllables,60, "dan")
table.insert(syllables,61, "cer")
table.insert(syllables,62, "vision")
table.insert(syllables,63, "cho")
table.insert(syllables,64, "late")
table.insert(syllables,65, "dig")
table.insert(syllables,66, "ger")
table.insert(syllables,67, "tri")
table.insert(syllables,68, "angle")
table.insert(syllables,69, "py")
table.insert(syllables,70, "mid")
table.insert(syllables,71, "tur")
table.insert(syllables,72, "tle")
table.insert(syllables,73, "au")
table.insert(syllables,74, "mo")
table.insert(syllables,75, "bile")
table.insert(syllables,76, "pig")
table.insert(syllables,77,  "wel")
table.insert(syllables,78, "re")
table.insert(syllables,79, "fri")
table.insert(syllables,80, "ge")
table.insert(syllables,81, "tor")
table.insert(syllables,82, "ba")
table.insert(syllables,83, "la")
table.insert(syllables,84, "lai")
table.insert(syllables,85, "ka")
table.insert(syllables,86, "blan")
table.insert(syllables,87, "ket")
table.insert(syllables,88, "the")
table.insert(syllables,89, "tics")
table.insert(syllables,90, "ca")
table.insert(syllables,91, "me")
table.insert(syllables,92, "fish")
table.insert(syllables,93, "ice")
table.insert(syllables,94, "cream")
table.insert(syllables,95, "fire")
table.insert(syllables,96, "ex")
table.insert(syllables,97, "tin")
table.insert(syllables,98, "gui")
table.insert(syllables,100, "cof")
table.insert(syllables,101, "fee")
table.insert(syllables,102, "ker")
table.insert(syllables,103, "prin")
table.insert(syllables,104, "cess")
table.insert(syllables,105, "sher")
-- 99 занят


--  храним слова и номера слогов, из которых они состоят
words = {}
table.insert(words,0, {"mommy", 0, 1, path = 0}) -- 0 - номер из syllables таблицы
table.insert(words,1, {"daddy", 2, 3, path = 1}) -- path - номер картинки lvl в папке			
table.insert(words,2, {"fishes", 99, 4, path = 2})			
table.insert(words,3, {"rabbit", 5, 6, path = 3})			
table.insert(words,4, {"teacup", 7, 8, path = 4})			
table.insert(words,5, {"kitty", 9, 10, path = 5})	
table.insert(words,6, {"doggie", 14, 15, path = 7})		
table.insert(words,7, {"rocket", 16, 17, path = 8})		
table.insert(words,8, {"rainbow", 18, 19, path = 9})		
table.insert(words,9, {"oak-tree", 20, 21, path = 10})		
--table.insert(words,11, {"корова", 20, 21, 22})		
table.insert(words,10, {"airplane", 22, 23, path = 12})
table.insert(words,11, {"steambot", 27, 28, path = 14})		
--table.insert(words,14, {"облако", 29, 30, 20})	
table.insert(words,12, {"girlie", 34, 35, path = 19})
table.insert(words,13, {"rhino", 29, 30, path = 16})		
table.insert(words,14, {"palette", 24, 31, path = 17})		
table.insert(words,15, {"pencil", 32, 33, path = 18})		
table.insert(words,16, {"hairbrush", 45, 46, path = 23})		
table.insert(words,17, {"orange", 47, 48, path = 24})		
table.insert(words,18, {"lorry", 49, 13, path = 25})
table.insert(words,19, {"monkey", 53, 54, path = 27})	
table.insert(words,20, {"piano", 55, 30, path = 28})
table.insert(words,21, {"bicycle", 56, 57, path = 31})	
table.insert(words,22, {"princess", 103, 104, path = 30})	
table.insert(words,23, {"triangle", 67, 68, path = 36})	
table.insert(words,24, {"turtle", 71, 72, path = 38})
table.insert(words,25, {"piglet", 76, 59, path = 40})	
table.insert(words,26, {"towel", 41, 77, path = 41})
table.insert(words,27, {"blanket", 86, 87, path = 44})
table.insert(words,28, {"digger", 65, 66, path = 35})
table.insert(words,29, {"ice-cream", 93, 94, path = 48})
table.insert(words,30, {"television", 51, 12, 62, path = 33})	
table.insert(words,31, {"chocolate", 63, 43, 64, path = 34})
table.insert(words,32, {"cutlery", 11, 12, 13, path = 6})				
table.insert(words,33, {"parachute", 24, 25, 26, path = 13})			
table.insert(words,34, {"magician", 36, 37, 38, path = 20})		
table.insert(words,35, {"potato", 39, 40, 41, path = 21})		
table.insert(words,36, {"crocodile", 42, 43, 44, path = 22})				
table.insert(words,37, {"antelope", 50, 51, 52, path = 26})	
table.insert(words,38, {"pyramid", 69, 25, 70, path = 37})	
table.insert(words,39, {"camera", 90, 91, 25, path = 46})
table.insert(words,40, {"cuttlefish", 11, 72, 92, path = 47})	
table.insert(words,41, {"coffeemaker", 100, 101, 36, 102, path = 29})	
table.insert(words,42, {"balletdancer", 58, 59, 60, 61, path = 32})			
table.insert(words,43, {"automobile", 73, 41, 74, 75, path = 39})	
table.insert(words,44, {"balalaika", 82, 83, 84, 85, path = 43})
table.insert(words,45, {"mathematics", 36, 88, 36, 89, path = 45})
table.insert(words,46, {"fire-extinguisher", 95, 96, 97, 98, 105, path = 49})	

table.insert(words,47, {"refrigerator", 78, 79, 80, 25, 81, path = 42})	


translations =
{
    ["app_name"] = "By syllables",
    ["about_text"] = "\"By syllables\" is specifically designed for a child's tablet computer PLAYPAD ® © BEST SYSTEM (HK) TRADING LIMITED,  appgranula.com, 2013",
    ["about_header"] = "About",   
    ["OK"] = "OK", 
    ["achieve_more"] = "Complete the previous levels!",
    ["audio"] = "audio",
    ["audio_off"] = "Sound off",
     ["audio_on"] = "Sound on",
     ["level"] = "level",
}

--[[
 {
        ["en"] = "   Collect the \nbroken pieces!",
        ["de"] = "Hallo",
        ["ru"] = "Собери осколки!",
        ["русский"] = "Собери осколки!",
        ["english"] = "   Collect the \nbroken pieces!", ---не
        ["английский"] = "   Collect the \nbroken pieces!", -- не
        ["English"] = "   Collect the \nbroken pieces!",
        ["1049"] = "Собери осколки!",
        ["zh"] = "搜集碎片"
    },
]]--


