--require "CiderDebugger";-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------
--Debug the smart way with Cider!
--start coding and press the debug button
-- hide the status bar


display.setStatusBar( display.HiddenStatusBar )

-- include the Corona "storyboard" module
local storyboard = require "storyboard"

--local core = require("core")


--[[
print(core.syllables[14])
print(core.words[40][1])

syllables = {}
table.insert(syllables,0,	"ма")
table.insert(syllables,1,	"па")

print( syllables[0])
]]--
-- load menu screen

local loadsave = require("loadsave")
local json_file_name = "by-syllabes-progress.json"

local json_progress = loadsave.loadTable(json_file_name)
 
--  первый раз запустили игру 
if json_progress == nil then
          json_progress = {}
          json_progress.level = 0   --  начнем с 0 уровня
          json_progress.audio = 1   --  есть звук
          
          loadsave.saveTable(json_progress, json_file_name)
end

print(json_progress.level)
storyboard.json_file_name = json_file_name
storyboard.json_progress = json_progress

storyboard.level =  0--json_progress.level  --  сохраним текущий уровень между сценами
print("main.lua current level " .. json_progress.level)
storyboard.can = true


local l_ = system.getPreference("ui", "language")
if l_ ~= "1049" then    --  работаем в винде?!
    local l2 = system.getPreference("locale", "language")--  работа на устройстве
    -- проверка на допустимые языки иначе используем en
    if l2 ~= 'en' and l2 ~= 'zh' and l2 ~= 'ru' then
        l2 = 'en'
    end
   -- l2 = 'zh'
   
   if l2 == 'zh' then
       l2 = 'en'
   end
    storyboard.lang = l2
    
    
--else storyboard.lang = l_ end   --  работа в винде
else storyboard.lang = "en" end   --  работа в винде
--storyboard.lang = "en"

local data_lang = require("local_" .. storyboard.lang)
storyboard.translations = data_lang.translations

--  в русской больше букв чем в англ ---- при переключении надо брать #data_lang.words 
if #data_lang.words < storyboard.level then
    storyboard.level = #data_lang.words 
    json_progress.level = storyboard.level
    loadsave.saveTable(json_progress, json_file_name)
end


storyboard.gotoScene( "menu" )

