module(..., package.seeall)

math.randomseed(os.time())

local storyboard = require("storyboard")
local loadsave = require("loadsave")

--  таблицы со связями слов и слогов 
local data_lang = require("local_" .. storyboard.lang)

local widget = require("widget")
function compare_tables(t1, t2)
  if #t1 ~= #t2 then return false end
  for i=1,#t1 do
    if t1[i] ~= t2[i] then return false end
  end
  return true
end

level = 0

local l_group -- группа из createScene
local image_background, d_gray, d_color

local detrituses = {} --  слоги

local physics = require("physics")
physics.start()
physics.setGravity(0, 0) -- чтоб вниз не падали 0  по X и 0 по Y ускорение ( смотрим как бы сверху, а не сбоку)

--  координаты, когда активируется стазис
local stasis_x = {}



table.insert(stasis_x, 2, {120, 360})
table.insert(stasis_x, 3, {80, 240, 400})
table.insert(stasis_x, 4, {60, 180, 300, 420})
table.insert(stasis_x, 5, {48, 144, 240, 336, 432})
local stasis_y = 730

--  в каком диапазоне ищем стазисные положения
local radius_x = {}
table.insert(radius_x, 2, 110)
table.insert(radius_x, 3, 80)
table.insert(radius_x, 4, 60)
table.insert(radius_x, 5, 40)
local radius_y = 60


 stasis_count = 0 -- число в стазисе
 perfect_stasis = {}
 current_stasis = {}
local function check_stasis(x, y)
 
end

local function check_audio_on_or_off()
    if storyboard.json_progress.audio == 1 then
        return true
    end
    return false
end

local audio_l = {}

local ext = ".mp3"
    if storyboard.lang == "en" then
        ext = ".wav"
    end

local function play_one_syllable(value)
    if check_audio_on_or_off() == false then
        return;
    end
  --  local a = audio.loadStream("audio/sl_" .. value .. ".mp3")
  --    audio.play(a)
    audio.play(audio_l[value])
end
local function play_win()
    if check_audio_on_or_off() == false then
        return;
    end
    
    if  storyboard.can == false then
        return
    end
    
    local d = audio.loadStream("audio/win.mp3")
    audio.play(d)   
end



local function play_word()
    if check_audio_on_or_off() == false then
        return;
    end
    
    
    local d = audio.loadStream("audio/audio-" .. storyboard.lang .. "/word_" .. level .. ext)
    timer.performWithDelay(1000, function() if storyboard.can then audio.play(d, {onComplete = play_win})end end) 
end



local function play_lose()
    if check_audio_on_or_off() == false then
        return;
    end
    local d = audio.loadStream("audio/lose.mp3")
    audio.play(d)     
end

gl_clock = nil

--  пройден уровень 
function level_complete()
    if storyboard.level < #data_lang.words then
        storyboard.level = storyboard.level + 1
    else storyboard.level = 1
        end
    print("core.lua new level " .. storyboard.level)
    if storyboard.level > storyboard.json_progress.level then   --  выйграли уровень: выйграли ли максимально достурный ранее?
        storyboard.json_progress.level = storyboard.level   --  сохраняем в json новый максимальный достигнутый уровень
        print("core.lua save new level " .. storyboard.json_progress.level)
        loadsave.saveTable(storyboard.json_progress, storyboard.json_file_name) --  сохраняем прогресс на флешке
    
    end
    
    transition.to(d_gray, {time = 1000, alpha = 0, onComplete = function() display.remove(d_gray); d_gray = nil; end})
    
    d_color = display.newImage("images/base/" .. data_lang.words[level]["path"] .. "_color.png")

    -- d_color:setReferencePoint( display.CenterReferencePoint )
    d_color.anchorX = 0.5
    d_color.anchorY = 0.5
    d_color.x, d_color.y = display.contentCenterX, display.contentCenterY
    d_color.alpha = 0
    
    --d_color:toBack()
   -- image_background:toBack()
    l_group:insert(d_color)

   -- transition.to(d_color, {time = 1000, alpha = 1, onComplete = function() gl_clock = timer.performWithDelay(3000, function() storyboard.gotoScene("transition_scene")   end)  end})
    transition.to(d_color, {time = 1000, alpha = 1, onComplete = function()  timer.performWithDelay(3000, function() if storyboard.can then storyboard.gotoScene("transition_scene") else storyboard.can = true end  end)  end})
     
end

local function ressurect()
    stasis_count = 0 -- число в стазисе
     perfect_stasis = {}
    current_stasis = {}
    detrituses = {}
audio_l = {}
end

local function check_if_any_detritus_already_in_this_position(i)
    local already_used = false
    for k = 1, #detrituses do
        if detrituses[k].now_at == i then
            already_used = true
        end
    end
    
    return already_used
end

--  уничтожаем прилепленный и показываем непрелепленный
local function handler_click(event)
    local t = event.target
    local r = t.link_to_target
    t.alpha = 0 
   -- display.remove(t)
   -- t = nil -- скрываем прилепленный
    --[[
    if r.touched == true then
        return true
    end
    r.touched = true
     ]]--
   -- transition.to(r, {time = 1000, alpha = 1, onComplete = function () r.touched = false end})-- раскрываем непрелепленный
    r.alpha = 1 
    --print("target alpha =1")
  
    
    if r.isBodyActive == false then
            --  даем ему возможность быть активным
            r.isBodyActive = true
            r.now_at = -1
            r.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
            -- выели из стазиса
            if stasis_count > 0 then
                stasis_count = stasis_count - 1
            end
            
            if r.stasis ~= nil then
              
                r.stasis.alpha = 0
               
             --   native.showAlert("stasis", "exist")
            end
          
          
        end
end

--  скрываем непрелепленный и показываем прелепленный
local function reveal_in_stasis(target)
   target.alpha = 0
   target.now_at = -1
   target.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
   local path_to_image = target.path .. "_1.png"
       -- print(str)
  -- local d = display.newImage(path_to_image)
  
  local str = data_lang.syllables[data_lang.words[level][target.k +1]]
  local fs = 34
  if string.len(str) > 4 then
      fs = 24
      
  end
   local d = widget.newButton{
                 label = str,
		defaultFile="images/detr_" .. target.col .. "_1.png",
		overFile="images/detr_" .. target.col .. "_1.png",
                font = "segoe",
                fontSize = fs,
                yOffset = -11,
                labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
                --onRelease = onAboutBtnRelease
	}
   d.x, d.y = target.x, target.y
   d.link_to_target = target
   d:addEventListener("touch", handler_click)
   l_group:insert(d)
   target.stasis = d
   target.x, target.y = math.random(90, 400), math.random(260, 650)
   target.alpha = 0
  -- print("target alpha =0 " .. target.alpha)
end


--  злой фикс: иногда у слогов после стазиса не обнуляется alpha
local function onFrame (event)
    if detrituses ~= nil then
    for i =1, #detrituses do
        if detrituses[i].stasis ~= nil then
        if detrituses[i].alpha == 1 and detrituses[i].stasis.alpha == 1 then
            print("hyi")
           -- native.showAlert("hyi", "hui")
            detrituses[i].alpha = 0
            print("changed to " .. detrituses[i].alpha)
        
        end
        end
        end
        end
end
-- update the position every frame
Runtime:addEventListener( "enterFrame", onFrame )


-- A basic function for dragging physics objects
local function startDrag( event )
        local t = event.target
 
        local phase = event.phase
       -- print("alpha " .. t.alpha)
       -- print("\n\nbefore " .. stasis_count)
        
        --  кликаем по слогу, уже закрепленному внизу
        if t.isBodyActive == false then
            --  даем ему возможность быть активным
            t.isBodyActive = true
            t.now_at = -1
            t.nead_value_to_be_here = -1 -- ушли из ячеки и забываем что ей нужно
            -- выели из стазиса
            if stasis_count > 0 then
                stasis_count = stasis_count - 1
            end
            
          
        end
        --------
        --print("after " .. stasis_count )
   
        
        
        if "began" == phase then
                play_one_syllable(t.value)
                display.getCurrentStage():setFocus( t )
                t.isFocus = true
 
                for i = 1, #detrituses do
                    if i ~= t.k then
                        detrituses[i].bodyType = "static"
                    end
                end
                t:toFront()   -- терь вверху группы    
                                
                -- Store initial position
                t.x0 = event.x - t.x
                t.y0 = event.y - t.y
                
                -- Make body type temporarily "kinematic" (to avoid gravitational forces)
               event.target.bodyType = "dynamic"
                
                -- Stop current motion, if any
                event.target:setLinearVelocity( 0, 0 )
                event.target.angularVelocity = 0
 
        elseif t.isFocus then
                if "moved" == phase then
                        t.x = event.x - t.x0
                        t.y = event.y - t.y0
                                
                        --  проверка чтобы не вылезло за границы при драге        
                        if t.x < 10 then t.x = 10
                        end
                        if t.x > 470 then t.x = 470
                        end
                        if t.y < 10 then t.y = 10
                        end
                        if t.y > 790 then t.y = 790
                        end    
                        event.target.bodyType = "kinematic"    -- игнорирование гравитации
                        
                elseif "ended" == phase or "cancelled" == phase then
                        display.getCurrentStage():setFocus( nil )
                        t.isFocus = false
                        
                        -- Switch body type back to "dynamic", unless we've marked this sprite as a platform
                        if ( not event.target.isPlatform ) then
                                event.target.bodyType = "dynamic"
                        end
                        
                        
                        ------------------------------------------------------
                      --  print("t.x " .. t.x)
                     --   print("t.y " .. t.y)
                        
                        local l =  #data_lang.words[level] -- {"математика", 0, 69, 0, 51, 7})
                        for i = 1, l-1 do
                            local x_ = stasis_x[#data_lang.words[level] - 1][i]
                           -- print("x_ " .. x_)
          
                            if math.abs(t.x - x_) < radius_x[#data_lang.words[level] - 1] and math.abs(t.y - stasis_y) < radius_y then
                               -- print("in stasis")
                               -- проверка на свободность ячейки
                               if check_if_any_detritus_already_in_this_position(i) == false then
                                t.x = x_
                                t.y = stasis_y
                                --t:removeEventListener("touch", startDrag)
                                --t.bodyType = "static"
                                
                                reveal_in_stasis(t)
                                
                                --  в стазисе слог не олжен взаимодействовать с другими до клика: пока по нему не кликнут
                                t.isBodyActive = false
                                --  еще 1 ячейка в стазисе
                                stasis_count = stasis_count + 1
                               
                                
                                t.now_at = i -- запоминаем в какой ячейке сейчас находимся: чтобы при передвижении других знать, что эта ячейка занята
                   
                               -- вошли в ячейку и запоминаем какое значение должно быть НА САМОМ ДЕЛЕ (нужно) ячейке
                                t.nead_value_to_be_here = data_lang.words[level][i+1]
                                
                                -- проверка на победу: если для всех слогов в ячейках 
                                -- значение нужное ячейке совпадает со значением слога
                                if stasis_count == l - 1 then
                                    print("all in stasis")
                                    local all_good = true
                                    for v = 1, #detrituses do
                                        -- проверяем по значениям
                                        if detrituses[v].nead_value_to_be_here ~= detrituses[v].value then
                                            
                                            all_good = false
                                        end
                                    end
                                    if all_good then
                                        print("yeeah")
                                        
                                        if storyboard.can then
                                        play_word()
                                        end
                                        
                                        level_complete()
                                        --native.showAlert("!", "yeeah")
                                        
                                        for y =1, #detrituses do
                                            detrituses[y]:removeEventListener("touch",startDrag)
                                            if detrituses[y].stasis ~= nil then
                                                detrituses[y].stasis:removeEventListener("touch",handler_click)
                                            end
                                        end
                                    else 
                                        print("ohhh")
                                        play_lose()
                                        -- native.showAlert("!", "ohhh")
                                        
                                        for y =1, #detrituses do
                                            detrituses[y]:removeEventListener("touch",startDrag)
                                              if detrituses[y].stasis ~= nil then
                                                detrituses[y].stasis:removeEventListener("touch",handler_click)
                                            end
                                        end
                                        timer.performWithDelay(1500, function() if storyboard.can then storyboard.gotoScene("transition_scene") else storyboard.can = true end end, 1)
                                    end
                                    
                                    
                                end
                            end
                        end
                        
                        end
                        ------------------------------------------------------
                        
                        
                        for i = 1, #detrituses do
                            if i ~= t.k then
                                detrituses[i].bodyType = "dynamic"
                            end
                        end
 
                end
        end
 
        -- Stop further propagation of touch event!
        return true
end


--  случайно размесить слоги
function level_randomly_seed_syllables(group)
    local d_count = #data_lang.words[level] -- первое будет само слово, образуемое слагами
    for i = 2, d_count do           -- поэтому начинаем с 2, а не с 1
       -- print(level)
      --  print(i)
        
        local path_to_image = "images/sl" .. data_lang.words[level][i] .. ".png"
       -- print(str)
        --local d = display.newImage(path_to_image)
        
        local str = data_lang.syllables[data_lang.words[level][i]]
        local fs = 40
        if string.len(str) > 4 then
            fs = 34

        end
        
        local col = math.random(1, 5)   --  цвет
        
        local d = widget.newButton{
                 label = str,
		defaultFile="images/detr_" .. col .. ".png",
		overFile="images/detr_" .. col .. ".png",
                font = "segoe",
                yOffset = -11,
                fontSize = fs,
                labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 1 } },
                --onRelease = onAboutBtnRelease
	}
        d.rotation = math.random(-20, 20)
        d.x, d.y = math.random(100, 340), math.random(260, 650)
        d.k = i - 1
       -- d.k = words[level][i]
       d.col = col
       d.value = data_lang.words[level][i]
       table.insert(audio_l, d.value, audio.loadStream("audio/audio-" .. storyboard.lang .. "/sl_" .. d.value .. ext))
        d.path = "images/sl" .. data_lang.words[level][i]
        d.number = data_lang.words[level][i]
        d.alpha = 0
        transition.to(d, {time = 1000, alpha = 1})-- раскрываем непрелепленный
       
        table.insert(detrituses, d)
        table.insert(perfect_stasis, data_lang.words[level][i]) -- запоминаем номера слогов
        d:addEventListener( "touch", startDrag )
        physics.addBody(d, "dynamic")
         d.isFixedRotation = true
        group:insert(d)
    end
        
end
--[[
function load_walls()
    local _W = display.contentWidth
    local _H = display.contentHeight
    local left_wall = display.newRect( -50, 0, 1, _H * 2)
    physics.addBody(left_wall, "static")

    local right_wall = display.newRect( _W + 10, 0, 50, _H * 2)
    physics.addBody(right_wall, "static")

    local up_wall = display.newRect(0, -10, _W, 20)
    physics.addBody(up_wall, "static")
    
    local down_wall = display.newRect(0, _H + 10, _W, 20)
    physics.addBody(down_wall, "static")
    
    l_group:insert(left_wall)
    l_group:insert(right_wall)
    l_group:insert(up_wall)
    l_group:insert(down_wall)
end
]]--
--  загрузка бекграунда и слогов
function load_level_data(group)
    l_group = group
    ressurect()    -- очищаем от предыдущего игрового уровня: различные таблицы становятся пустыми
    --  бекграунд
    image_background = display.newImage( "images/background.png")
    -- image_background:setReferencePoint( display.CenterReferencePoint )
    image_background.anchorX = 0.5
    image_background.anchorY = 0.5
    image_background.x, image_background.y = display.contentCenterX, display.contentCenterY  
    group:insert(image_background)
     
     if level == 0 then
        image_task =  display.newImage( "images/ui-" .. storyboard.lang .. "/backslot.png")
        -- image_task:setReferencePoint( display.CenterReferencePoint )
        image_task.anchorX = 0.5
        image_task.anchorY = 0.5
        image_task.x, image_task.y = display.contentCenterX, display.contentHeight - 50
        group:insert(image_task)
    end
     
    -- серая картинка (заменится на ицветную в случае победы) 
    d_gray = display.newImage("images/base/" .. data_lang.words[level]["path"] .. ".png")
    -- d_gray:setReferencePoint( display.CenterReferencePoint )
    d_gray.anchorX = 0.5
    d_gray.anchorY = 0.5
    d_gray.x, d_gray.y = display.contentCenterX, display.contentCenterY
    group:insert(d_gray)
    
    --  размещение статических препятствий вокруг экрана, чтоб слоги не вылезали
   -- create wall objects
    local topWall = display.newRect( -40, -40, display.contentWidth, -60 )
    topWall.anchorX = 0
    topWall.anchorY = 0
    local bottomWall = display.newRect( -40, display.contentHeight + 40 , display.contentWidth, display.contentHeight + 60 )
    bottomWall.anchorX = 0
    bottomWall.anchorY = 0
    local leftWall = display.newRect( -40, -40, -30, display.contentHeight + 40 )
    leftWall.anchorX = 0
    leftWall.anchorY = 0
    local rightWall = display.newRect( display.contentWidth + 30, 0, display.contentWidth + 60, display.contentHeight  )
    rightWall.anchorX = 0
    rightWall.anchorY = 0

    -- make them physics bodies
    physics.addBody(topWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(bottomWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(leftWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    physics.addBody(rightWall, "static", {density = 1.0, friction = 0, bounce = 1, isSensor = false})
    
    --  случайное размещение слогов + dynamic физика на них
    level_randomly_seed_syllables(group)
end

--  проверка на правильность расположения слогов
function level_check()
    
end


